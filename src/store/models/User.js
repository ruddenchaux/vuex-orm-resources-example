import { Model, PathParam, Http } from '@vuex-orm/core'
import Todo from './Todo'

export default class User extends Model {
  static entity = 'users'

  static _conf = {
    // "baseUrl": "",
    "endpointPath": "/user_override",
    "methods": [
      {
        "name": "assignTo",
        "remote": true,
        "http": {
            "path": "/{self}/assignTo",
            "method": "post"
        }
      },
      {
        "name": "findById",
        "remote": false,
        "http": {
            "path": "/{self}/:id",
            "method": "post"
        }
      },
      {
        "name": "exist",
        "remote": false,
        "http": {
            "path": "/{self}/exist/:id",
            "method": "post"
        }
      },
      {
        "name": "countTotal",
        "remote": true,
        "http": {
            "path": "/{self}/countTotal",
            "method": "get"
        }
      }
    ]
  }

  static fields () {
    return {
      id: this.attr(null),
      name: this.attr(''),
      todos: this.hasMany(Todo, 'user_id')
    }
  }

  static staticBusinessFunc1() {
    console.log('user staticBusinessFunc1');
  }

  static async existOverride(id, conf= this.getMethodConf('existOverride')) {
    const _conf = this.checkMethodConf('existOverride', conf)
    let data

    if (_conf.remote) {
      const url = this.getUrl(_conf, new PathParam('id', id.toString()))
      data = await Http[_conf.http.method](url)
      .catch((err) => { console.log(err); }) || []

      if(_conf.localSync) {
        // await this.dispatch('insertOrUpdate', { data })
      }
    }

    return {}
  }
}
