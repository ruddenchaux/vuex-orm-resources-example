import Vue from 'vue'
import Vuex from 'vuex'

// modulo ORM (Object-Relational Mapping) Resources gestisce i dati utilizzati dall'applicazione
// Interazione con il server api
// Interazione nello store vuex
// Manipolazione dei dati con ORM
import VuexORMResources from '@vuex-orm/core'

// modulo per la persistenza dello store
// localStorage, sessionStorage, localforage or your custom Storage object. 
// Must implement getItem, setItem, clear etc. 
// Default: window.localStorage
import VuexPersistence from 'vuex-persist'

import database from './database'

Vue.use(Vuex)

// configurazione del modulo che gestisce la persistenza dello store
//////////////////////////////////////////////////////////////////////////
let vuexPersistenceConfig = {}

// per strutturare il salavatoaggio dello store 
const saveState = (key, state, storage) => {
  console.log(state);
  console.log(storage);
}

if(window.cordova) {
  vuexPersistenceConfig.storage = window.sessionStorage;
  vuexPersistenceConfig.saveState = saveState;
}
else {
  vuexPersistenceConfig.storage = window.sessionStorage;
}

const vuexLocal = new VuexPersistence(vuexPersistenceConfig)
/////////////////////////////////////////////////////////////////////////

// configurazione dello store vuex
const store = new Vuex.Store({
  plugins: [
    VuexORMResources.install(database, {
      namespace: 'entities',
      resources: {
        baseUrl: 'http://localhost:3000/api'
      }
    }), 
    vuexLocal.plugin
  ]
})

export default store
